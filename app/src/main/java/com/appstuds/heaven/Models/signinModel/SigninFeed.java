package com.appstuds.heaven.Models.signinModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by osr on 22/3/18.
 */

public class SigninFeed {


    @Expose
    @SerializedName("response")
    public Response response;
    @Expose
    @SerializedName("status")
    public int status;

    public static class Response {
        @Expose
        @SerializedName("userProfile")
        private Userprofile userprofile;
        @Expose
        @SerializedName("accessToken")
        private String accesstoken;
        @Expose
        @SerializedName("message")
        private String message;

        public Userprofile getUserprofile() {
            return userprofile;
        }

        public void setUserprofile(Userprofile userprofile) {
            this.userprofile = userprofile;
        }

        public String getAccesstoken() {
            return accesstoken;
        }

        public void setAccesstoken(String accesstoken) {
            this.accesstoken = accesstoken;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class Userprofile {
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("_id")
        private String Id;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }
}
