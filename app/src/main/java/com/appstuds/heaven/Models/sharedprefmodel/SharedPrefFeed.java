package com.appstuds.heaven.Models.sharedprefmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by osr on 22/3/18.
 */

public class SharedPrefFeed {


    @Expose
    @SerializedName("response")
    public java.util.List<Response> response;
    @Expose
    @SerializedName("status")
    public int status;

    public static class Response {
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("_id")
        private String Id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }
}
