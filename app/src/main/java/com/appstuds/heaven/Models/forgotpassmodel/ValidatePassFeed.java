package com.appstuds.heaven.Models.forgotpassmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by osr on 26/3/18.
 */

public class ValidatePassFeed {


    @Expose
    @SerializedName("response")
    public Response response;
    @Expose
    @SerializedName("status")
    public int status;

    public static class Response {
        @Expose
        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
