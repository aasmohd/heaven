package com.appstuds.heaven.Models;

import com.appstuds.heaven.Models.sharedprefmodel.SharedPrefFeed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by osr on 22/3/18.
 */

public class Getter {
    private static final Getter ourInstance = new Getter();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String email;
    public String password;

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String bio;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPrefferedname() {
        return prefferedname;
    }

    public void setPrefferedname(String prefferedname) {
        this.prefferedname = prefferedname;
    }

    public String firstname;
    public String lastname;
    public String prefferedname;


    public List<SharedPrefFeed.Response> getInterest() {
        return interest;
    }

    public void setInterest(List<SharedPrefFeed.Response> interest) {
        this.interest = interest;
    }

    public List<SharedPrefFeed.Response> interest;



    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String prefix;

    public String getLogintype() {
        return logintype;
    }

    public void setLogintype(String logintype) {
        this.logintype = logintype;
    }

    public String logintype;

    public static Getter getInstance() {
        return ourInstance;
    }

    private Getter() {
    }

    /////// for differentiating b/w mybusiness and myself

    public int getBusiness_or_self() {
        return business_or_self;
    }

    public void setBusiness_or_self(int business_or_self) {
        this.business_or_self = business_or_self;
    }

    public int business_or_self;


}
