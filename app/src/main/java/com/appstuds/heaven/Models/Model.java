package com.appstuds.heaven.Models;

public class Model {

    public static final int MSG_TYPE_SENT=0;
    public static final int MSG_TYPE_RECEIVED=1;

    public int type;
    public String text;
    public int count;

    public Model(int type, String text,int count)
    {
        this.type=type;
        this.text=text;
        this.count=count;
    }
}