package com.appstuds.heaven.Models.validateemailfeed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by osr on 2/4/18.
 */

public class ValidateEmailFeed {


    @Expose
    @SerializedName("error")
    public Error error;
    @Expose
    @SerializedName("status")
    public int status;

    public static class Error {
        @Expose
        @SerializedName("errors")
        public List<Errors> errors;
        @Expose
        @SerializedName("message")
        public String message;
        @Expose
        @SerializedName("errorCode")
        public int errorCode;
    }

    public static class Errors {
        @Expose
        @SerializedName("message")
        public String message;
        @Expose
        @SerializedName("fieldName")
        public String fieldName;
    }
}
