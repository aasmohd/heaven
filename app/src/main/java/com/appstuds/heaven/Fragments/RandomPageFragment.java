package com.appstuds.heaven.Fragments;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.appstuds.heaven.R;
import com.appstuds.heaven.Utils.Metrics;

import java.util.Random;


import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class RandomPageFragment extends Fragment {
	public final static String TEXT = "TEXT";
	static int[] drawable = new int[]{R.drawable.man, R.drawable.small, R.drawable.graphic_image, R.drawable.man};

	private String text;

	public RandomPageFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		text = getArguments().getString(TEXT);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		return createCircularImageView(getActivity(), 30, 30);
	}

	public static View createView(Context context, int widthDp, int heightDp, String text) {
//		FrameLayout l = new FrameLayout(context);
//		l.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		Log.d("page", "w = " + widthDp + ", h = " + heightDp);

		TextView textView = new TextView(context);
		textView.setId(R.id.carousel_child_container);
		textView.setLayoutParams(new FrameLayout.LayoutParams(
				widthDp > 0 ? (int) Metrics.convertDpToPixel(widthDp, context) : ViewGroup.LayoutParams.MATCH_PARENT,
				heightDp > 0 ? (int) Metrics.convertDpToPixel(heightDp, context) : ViewGroup.LayoutParams.MATCH_PARENT
		));
		textView.setGravity(Gravity.CENTER);
		textView.setTextSize(48);

		initializeTextView(textView, Integer.parseInt(text));

//		((FrameLayout.LayoutParams) textView.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
//		l.addView(textView);

//		return l;
		return textView;
	}
	/*public static View createTextView(Context context, int widthDp, int heightDp){
        TextView textView = new TextView(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textView.setElevation(10);
        }
        textView.setId(R.id.text_view);
        textView.setAlpha((float) 0.2);
        textView.setLayoutParams(new FrameLayout.LayoutParams(
                widthDp > 0 ? (int) Metrics.convertDpToPixel(widthDp, context) : ViewGroup.LayoutParams.MATCH_PARENT,
                heightDp > 0 ? (int) Metrics.convertDpToPixel(heightDp, context) : ViewGroup.LayoutParams.MATCH_PARENT
        ));
	    return textView;
    }*/
	public static View createCircularImageView(Context context, int widthDp, int heightDp) {
//		FrameLayout l = new FrameLayout(context);
//		l.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		Log.d("page", "w = " + widthDp + ", h = " + heightDp);


        CircleImageView circleImageView = new CircleImageView(context);
        circleImageView.setId(R.id.carousel_child_container);
        circleImageView.setLayoutParams(new FrameLayout.LayoutParams(
				widthDp > 0 ? (int) Metrics.convertDpToPixel(widthDp, context) : ViewGroup.LayoutParams.MATCH_PARENT,
				heightDp > 0 ? (int) Metrics.convertDpToPixel(heightDp, context) : ViewGroup.LayoutParams.MATCH_PARENT
		));

        /*circleImageView.setGravity(Gravity.CENTER);
		textView.setTextSize(48);*/

		//initializeTextView(textView, Integer.parseInt(text));

//		((FrameLayout.LayoutParams) textView.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
//		l.addView(textView);

//		return l;
		return circleImageView;
	}

	public static void initializeTextView(TextView textView, int num) {
		int bgColor = randomColor(num);
		textView.setText("" + num);
		textView.setBackgroundColor(bgColor);
		textView.setTextColor(getContrastColor(bgColor));
	}
	public static void initializeImageView(CircleImageView circleImageView, int num) {
		int bgColor = randomImage(num);
        circleImageView.setImageResource(drawable[num]);

	}

	private static int randomColor(int seed) {
		return new Random(seed).nextInt(0x1000000) + 0xFF000000;
	}
	private static int randomImage(int seed) {
		return new Random(seed).nextInt(3);
	}

	private static int getContrastColor(int color) {
		return Color.rgb(255 - Color.red(color), 255 - Color.green(color), 255 - Color.blue(color));
	}


}
