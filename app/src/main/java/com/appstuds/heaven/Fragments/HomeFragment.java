package com.appstuds.heaven.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_home;
import com.appstuds.heaven.Fragments.PeopleBusinessFragment;
import com.appstuds.heaven.R;


public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_homee, container, false);
        ((HomeActivity) getActivity()).themechange(true);
        return root;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // getContext().getTheme().applyStyle(R.style.SplashTheme, true);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_home);
        EditText editText = (EditText) view.findViewById(R.id.clickabletoolbarhome);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PeopleBusinessFragment fragment = new PeopleBusinessFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();

            }
        });

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_home(getActivity());
        recyclerView.setAdapter(adapter);

    }
}
