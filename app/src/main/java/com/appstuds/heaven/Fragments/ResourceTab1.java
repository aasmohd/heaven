package com.appstuds.heaven.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.appstuds.heaven.Activities.DetailsActivity;
import com.appstuds.heaven.Activities.PrivacyPolicyActivity;
import com.appstuds.heaven.Activities.TermsofUseActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_ResTab1;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Tab1;
import com.appstuds.heaven.R;

/**
 * Created by H.P on 2/24/2018.
 */

public class ResourceTab1 extends Fragment {
    View root;

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.resource_tab1, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_resourcetab1);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_ResTab1(getActivity());
        recyclerView.setAdapter(adapter);

//        ImageView first_image_res=(ImageView) view.findViewById(R.id.first_image_in_res);
//        first_image_res.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(getActivity(), DetailsActivity.class);
//                startActivity(intent);
//
//
//
//            }
//        });



    }


}
