package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Discussion_open;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;

/**
 * Created by osr on 28/3/18.
 */

public class DiscussionForumOpenFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;
    PassSelector passData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View root=inflater.inflate(R.layout.activity_discussion_forum_open,container,false);
        ((HomeActivity) getActivity()).themechange(false);
       return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageView=(ImageView)view.findViewById(R.id.discussion_open_back);
        passData=(PassSelector)getActivity();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              passData.passData(1);
            }
        });
        //   carouselView = (CarouselView)findViewById(R.id.carousel_view);
        //  setCarsoleView();


        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_discussion_open);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Discussion_open(getActivity());
        recyclerView.setAdapter(adapter);

    }
}
