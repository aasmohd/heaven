package com.appstuds.heaven.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Fragments.*;
//import com.appstuds.heaven.Fragments.ResourceFragment;
import com.appstuds.heaven.R;


public class InnerResourceFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_resource, container, false);
        ((HomeActivity) getActivity()).themechange(false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {

                if (arg0==1)
                {
//                    Intent intent=new Intent(getActivity(),MyProfileActivity.class);
//                    startActivity(intent);
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {


            }

            @Override
            public void onPageScrollStateChanged(int num) {


            }
        });
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        CharSequence[] title = {"FEATURED", "ENTERTAINMENT","SHOPPING","MORE"};

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ResourceTab1();
                case 1:
                    return new ResourceTab2();
                case 2:
                    return new ResourceTab2();
                case 3:
                    return new ResourceTab2();
            }
            return new ResourceTab1();
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }
}
