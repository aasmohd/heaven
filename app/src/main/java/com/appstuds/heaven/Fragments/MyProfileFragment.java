package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_myprofile;
import com.appstuds.heaven.R;

/**
 * Created by osr on 9/3/18.
 */

public class MyProfileFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_profile, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ////android:theme="@style/SplashTheme"

        ((HomeActivity) getActivity()).themechange(true);
        //getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        //  getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Button button = (Button) view.findViewById(R.id.post_button_my_profile);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /////////////here
//                Intent intent=new Intent(getActivity(), HomeFirstpostActivity.class);
//                startActivity(intent);

                HomeFirstPostFragment fragment = new HomeFirstPostFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_my_profile);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_myprofile(getActivity());
        recyclerView.setAdapter(adapter);
    }


}
