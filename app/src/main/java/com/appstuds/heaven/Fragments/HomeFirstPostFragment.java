package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;

/**
 * Created by osr on 2/4/18.
 */

public class HomeFirstPostFragment extends Fragment {
    PassSelector passData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home_firstpost,container,false);
        ((HomeActivity) getActivity()).themechange(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageView=(ImageView)view.findViewById(R.id.home_first_post_back);
        passData=(PassSelector)getActivity();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passData.passData(1);
            }
        });
    }
}
