package com.appstuds.heaven.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.appstuds.heaven.Activities.ChatActivity;
import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_home;
import com.appstuds.heaven.Adapters.RecyclerAdapter_profile;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;


public class ProfileFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;
    PassSelector passData;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_profile, container, false);
        ((HomeActivity) getActivity()).themechange(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageView=(ImageView)view.findViewById(R.id.profile_back);
        Button connect_btn=(Button)view.findViewById(R.id.profile_connect_btn);
        passData=(PassSelector)getActivity();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               passData.passData(1);
            }
        });
        connect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ChatActivity.class);
                startActivity(intent);

            }
        });
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_profile);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_profile(getActivity());
        recyclerView.setAdapter(adapter);

    }
}
