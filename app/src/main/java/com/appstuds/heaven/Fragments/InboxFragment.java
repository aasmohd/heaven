package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Adapters.RecyclerAdapter_inbox;
import com.appstuds.heaven.R;

/**
 * Created by osr on 9/3/18.
 */

public class InboxFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_inbox, container, false);
        ((HomeActivity) getActivity()).themechange(false);
        return root;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_inbox);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_inbox(getActivity());
        recyclerView.setAdapter(adapter);


    }
}
