package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.R;

/**
 * Created by osr on 29/3/18.
 */

public class PeopleBusinessFragment extends Fragment {
    public static TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_people_business_, container, false);

        ((HomeActivity) getActivity()).themechange(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        HomeFragmentLaunch();
    }


    private void HomeFragmentLaunch() {
        PeopleBusinessHomeFragment fragment = new PeopleBusinessHomeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_res, fragment);
        ft.commit();
    }

}
