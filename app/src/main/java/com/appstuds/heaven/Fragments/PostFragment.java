package com.appstuds.heaven.Fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;
import com.appstuds.heaven.interfaces.ShowBottomBar;

/**
 * Created by osr on 9/3/18.
 */

public class PostFragment extends Fragment implements View.OnClickListener {

    ImageView addphoto, addvideo, back;
    Integer REQUEST_CAMERA_PHOTO = 0, SELECT_FILE_PHOTO = 2, REQUEST_CAMERA_VIDEO = 1, SELECT_FILE_VIDEO = 3;

    PassSelector passData;
    ShowBottomBar showBottomBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_post, container, false);
        ((HomeActivity) getActivity()).themechange(false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ((HomeActivity) getActivity()).showVisibleBottomBar(true);
        passData = (PassSelector) getActivity();
        showBottomBar = (ShowBottomBar) getActivity();
        showBottomBar.displayBottomBar();
        addphoto = (ImageView) view.findViewById(R.id.first_image_in_post);
        addvideo = (ImageView) view.findViewById(R.id.video_icon);
        back = (ImageView) view.findViewById(R.id.frag_post_back);
        back.setOnClickListener(this);
        addphoto.setOnClickListener(this);
        addvideo.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.first_image_in_post:
                SelectImage();
                break;
            case R.id.video_icon:
                SelectVideo();
                break;
            case R.id.frag_post_back:
                ((HomeActivity) getActivity()).showVisibleBottomBar(false);
                passData.passData(1);
                break;
        }

    }


    private void SelectImage() {
        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (items[which].equals("Camera")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA_PHOTO);
                } else if (items[which].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE_PHOTO);

                } else if (items[which].equals("cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void SelectVideo() {
        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Video");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (items[which].equals("Camera")) {

                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA_VIDEO);
                } else if (items[which].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE_VIDEO);

                } else if (items[which].equals("cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA_VIDEO) {

                Uri selectedVideoUri = data.getData();

                Toast.makeText(getActivity(), "video_added", Toast.LENGTH_SHORT).show();

            } else if (requestCode == SELECT_FILE_VIDEO) {
                Uri selectedImageUri = data.getData();
                // ivImage.setImageURI(selectedImageUri);
            } else if (requestCode == REQUEST_CAMERA_PHOTO) {
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                // ivImage.setImageBitmap(bmp);
                Toast.makeText(getActivity().getApplicationContext(), "Image Added", Toast.LENGTH_SHORT).show();

            } else if (requestCode == SELECT_FILE_PHOTO) {
                Uri selectedImageUri = data.getData();
                Toast.makeText(getActivity().getApplicationContext(), "Image Added", Toast.LENGTH_SHORT).show();
                //  ivImage.setImageURI(selectedImageUri);

            }
        }
    }


}
