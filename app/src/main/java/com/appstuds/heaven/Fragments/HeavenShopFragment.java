package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appstuds.heaven.Activities.*;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Shop;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;

/**
 * Created by osr on 9/3/18.
 */

public class HeavenShopFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public GridLayoutManager layoutManager;

    PassSelector passdata;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_heaven_shop, container, false);
        ((HomeActivity) getActivity()).themechange(false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_shop);
        ImageView back_image = (ImageView) view.findViewById(R.id.shop_back);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Shop(getActivity());
        recyclerView.setAdapter(adapter);
        passdata = (PassSelector) getActivity();

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                passdata.passData(1);



            }
        });


    }
}
