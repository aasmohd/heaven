package com.appstuds.heaven.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appstuds.heaven.Adapters.RecyclerAdapter_Tab2;
import com.appstuds.heaven.R;

/**
 * Created by H.P on 2/24/2018.
 */

public class Tab2 extends Fragment {
    View root;
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root=inflater.inflate(R.layout.fragment_tab2,container,false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view_tab2);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Tab2(getActivity());
        recyclerView.setAdapter(adapter);
        return  root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        HomeFragmentLaunch();
    }


//    private void HomeFragmentLaunch() {
//
//        FragmentInner2 fragment = new FragmentInner2();
//        FragmentManager fm = getActivity().getSupportFragmentManager();
//        FragmentTransaction ft = fm.beginTransaction();
//        ft.replace(R.id.innercontainer2_main, fragment);
//        ft.commit();
//    }
}
