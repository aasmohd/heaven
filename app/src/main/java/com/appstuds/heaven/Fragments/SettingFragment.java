package com.appstuds.heaven.Fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.appstuds.heaven.Activities.*;
import com.appstuds.heaven.Adapters.RecyclerAdapter_profile;
import com.appstuds.heaven.R;
import com.appstuds.heaven.interfaces.PassSelector;

/**
 * Created by osr on 9/3/18.
 */

public class SettingFragment extends Fragment{

    PassSelector passdata;
    CardView OpenChangePassActivity;
    Switch switch_setting;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        ((HomeActivity) getActivity()).themechange(false);
        return root;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        passdata=(PassSelector)getActivity();
        OpenChangePassActivity=(CardView)view.findViewById(R.id.open_changepass_activity);
        OpenChangePassActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChangePassword.class);
                startActivity(intent);


            }
        });

        switch_setting = (Switch)view.findViewById(R.id.switch_setting);

        switch_setting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position

                if (isChecked) {
                    switch_setting.getTrackDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.light_green), PorterDuff.Mode.SRC_IN);
                } else {
                    switch_setting.getTrackDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.gradient), PorterDuff.Mode.SRC_IN);
                }
            }
        });





//        CardView cardView_terms_of_ser=(CardView)view.findViewById(R.id.cardview2_setting);
//        cardView_terms_of_ser.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(getActivity(), TermsofUseActivity.class);
//                startActivity(intent);
//
//
//
//            }
//        });


        CardView cardView_terms_of_ser=(CardView)view.findViewById(R.id.cardview2_setting);
        cardView_terms_of_ser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TermsOfUseFragment fragment = new TermsOfUseFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();



            }
        });

//        CardView privacy_pol=(CardView)view.findViewById(R.id.cardview_privacy_pol);
//        privacy_pol.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(getActivity(), PrivacyPolicyActivity.class);
//                startActivity(intent);
//
//
//
//            }
//        });

        CardView privacy_pol=(CardView)view.findViewById(R.id.cardview_privacy_pol);
        privacy_pol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrivacyPolicyFragment fragment = new PrivacyPolicyFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });

        ImageView back=(ImageView)view.findViewById(R.id.frag_setting_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passdata.passData(1);

            }
        });














    }

}
