package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.heaven.Activities.DiscussionFormActivity;
import com.appstuds.heaven.R;


public class RecyclerAdapter_Shop extends RecyclerView.Adapter<RecyclerAdapter_Shop.RecyclerViewHolder> {

    Context context;
    View view;




    public RecyclerAdapter_Shop(Context context) {

        this.context = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata_shop, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

       // holder.chatPersonName.setText("Byrant Twiford");
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context,DiscussionFormActivity.class);
//                context.startActivity(intent);
//            }
//        });


    }

    @Override
    public int getItemCount() {

        return 2;

    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView chatPersonName,chatPersonName1;
        ImageView chatPersonImage,chatPersonImage1;
        CardView cardView;

        public RecyclerViewHolder(View View) {
            super(View);
            chatPersonName = (TextView) View.findViewById(R.id.chatpersonname);
            chatPersonImage = (ImageView) View.findViewById(R.id.chatpersonimage);
            chatPersonName1 = (TextView) View.findViewById(R.id.chatpersonname1);
            chatPersonImage1 = (ImageView) View.findViewById(R.id.chatpersonimage1);
            cardView=(CardView)View.findViewById(R.id.cardview1);

        }
    }



}

