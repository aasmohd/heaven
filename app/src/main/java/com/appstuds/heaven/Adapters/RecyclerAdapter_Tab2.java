package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.heaven.Activities.BusinessProfileActivity;
import com.appstuds.heaven.Activities.InboxActivity;
import com.appstuds.heaven.R;


public class RecyclerAdapter_Tab2 extends RecyclerView.Adapter<RecyclerAdapter_Tab2.RecyclerViewHolder> {

    Context context;
    View view;
    int [] img_res={R.drawable.man,R.drawable.man};




    public RecyclerAdapter_Tab2(Context context) {

        this.context = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata2, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

//        holder.chatPersonName.setText("Byrant Twiford");
//        holder.chatPersonImage.setImageResource(img_res[0]);
//        holder.chatPersonName1.setText("Byrant Twiford");
//        holder.chatPersonImage1.setImageResource(img_res[1]);

        holder.chatPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,BusinessProfileActivity.class);
                context.startActivity(intent);
            }
        });

        holder.chatPersonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,BusinessProfileActivity.class);
                context.startActivity(intent);
            }
        });

        holder.chatPersonImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,BusinessProfileActivity.class);
                context.startActivity(intent);
            }
        });

        holder.chatPersonName1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,BusinessProfileActivity.class);
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {

        return 2;

    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView chatPersonName,chatPersonName1;
        ImageView chatPersonImage,chatPersonImage1;

        public RecyclerViewHolder(View View) {
            super(View);
            chatPersonName = (TextView) View.findViewById(R.id.chatpersonname);
            chatPersonImage = (ImageView) View.findViewById(R.id.chatpersonimage);
            chatPersonName1 = (TextView) View.findViewById(R.id.second_chatpersonname);
            chatPersonImage1 = (ImageView) View.findViewById(R.id.second_chatpersonimage);

        }
    }



}
