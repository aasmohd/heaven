package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.heaven.R;
import com.igalata.bubblepicker.model.Color;

import java.util.ArrayList;

/**
 * Created by osr on 14/3/18.
 */

public class CustomAdapterSpinner extends BaseAdapter

{
        Context context;
        int flags[];
        ArrayList<String> countryNames;
        LayoutInflater inflter;

        public CustomAdapterSpinner(Context applicationContext, ArrayList<String> countryNames) {
            this.context = applicationContext;

            this.countryNames = countryNames;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return countryNames.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = (TextView) view.findViewById(R.id.textView_spinner);
            view.setPadding(10, view.getPaddingTop(),view.getPaddingRight(), view.getPaddingBottom());
            names.setText(countryNames.get(i));
            return view;
        }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view = super.getView(position, convertView, parent);
//        view.setPadding(0, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
//        return view;
//    }
    }

