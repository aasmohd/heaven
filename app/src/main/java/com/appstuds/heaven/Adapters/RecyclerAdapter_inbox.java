package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.heaven.Activities.ChatActivity;
import com.appstuds.heaven.Activities.HeavenShopActivity;
import com.appstuds.heaven.R;

/**
 * Created by osr on 28/2/18.
 */


public class RecyclerAdapter_inbox extends RecyclerView.Adapter<RecyclerAdapter_inbox.RecyclerViewHolder> {

    Context context;
    View view;
    int [] img_res={R.drawable.byrant,R.drawable.byrant};




    public RecyclerAdapter_inbox(Context context) {

        this.context = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata_inbox, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

//        holder.chatPersonName.setText("Byrant Twiford");
//        holder.chatPersonImage.setImageResource(img_res[0]);
//        holder.chatPersonName1.setText("Byrant Twiford");
//        holder.chatPersonImage1.setImageResource(img_res[1]);
        holder.chatPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });
        holder.chatPersonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });


        holder.chatPersonImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });
        holder.chatPersonName1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });


        holder.chatPersonImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });
        holder.chatPersonName2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ChatActivity.class);
                context.startActivity(intent);
            }
        });




    }

    @Override
    public int getItemCount() {

        return 2;

    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView chatPersonName,chatPersonName1,chatPersonName2;
        ImageView chatPersonImage,chatPersonImage1,chatPersonImage2;

        public RecyclerViewHolder(View View) {
            super(View);
            chatPersonName = (TextView) View.findViewById(R.id.chatpersonname);
            chatPersonImage = (ImageView) View.findViewById(R.id.chatpersonimage);
            chatPersonName1 = (TextView) View.findViewById(R.id.chatpersonname1);
            chatPersonImage1 = (ImageView) View.findViewById(R.id.chatpersonimage1);

            chatPersonName2 = (TextView) View.findViewById(R.id.second_chatpersonname1);
            chatPersonImage2 = (ImageView) View.findViewById(R.id.second_chatpersonimage1);

        }
    }



}
