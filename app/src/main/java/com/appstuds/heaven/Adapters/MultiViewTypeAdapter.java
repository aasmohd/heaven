package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appstuds.heaven.Models.Model;
import com.appstuds.heaven.R;

import java.util.ArrayList;

public class MultiViewTypeAdapter extends RecyclerView.Adapter {

    private ArrayList<Model> dataSet;
    Context mContext;
        int [] img_res={R.drawable.byrant_specs,R.drawable.johny_colclough_pic};

    public MultiViewTypeAdapter(ArrayList<Model> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //3
        View view;
        switch (viewType) {
            case Model.MSG_TYPE_SENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_right, parent, false);
                return new SentTypeViewHolder(view);
            case Model.MSG_TYPE_RECEIVED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_left, parent, false);
                return new ReceivedTypeViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        //2

        switch (dataSet.get(position).type) {
            case 0:
                return Model.MSG_TYPE_SENT;
            case 1:
                return Model.MSG_TYPE_RECEIVED;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {


        Model object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case Model.MSG_TYPE_SENT:

                    if (dataSet.get(listPosition).count==0) {

                        ((SentTypeViewHolder) holder).txtType.setText(object.text);
                        ((SentTypeViewHolder) holder).send_right_pic.setImageResource(img_res[1]);

                    }
                    else {

                        ((SentTypeViewHolder) holder).txtType.setText(object.text);
                        ((SentTypeViewHolder) holder).send_right_pic.setVisibility(View.GONE);

                    }


                    break;
                case Model.MSG_TYPE_RECEIVED:
                    ((ReceivedTypeViewHolder) holder).txtType.setText(object.text);
                    ((ReceivedTypeViewHolder) holder).received_left_pic.setImageResource(img_res[0]);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();//1
    }

    public static class SentTypeViewHolder extends RecyclerView.ViewHolder {
        TextView txtType;
        ImageView send_right_pic;
        LinearLayout linearLayout;

        public SentTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            this.send_right_pic=(ImageView)itemView.findViewById(R.id.sent_right_pic);
        }
    }

    public static class ReceivedTypeViewHolder extends RecyclerView.ViewHolder {
        TextView txtType;
        ImageView received_left_pic;

        public ReceivedTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.received_left_pic=(ImageView)itemView.findViewById(R.id.received_left_pic);
        }
    }
}

