package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appstuds.heaven.Utils.ActivityCountryList;
import com.appstuds.heaven.Utils.Countries;
import com.appstuds.heaven.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dragonfly on 6/4/2016.
 */
@SuppressWarnings("ALL")
public class CountryAdapter extends BaseAdapter implements Filterable {

    LayoutInflater layoutInflater;
    private Context context;
    private List<Countries> countriesList;
    private List<Countries> searchList;
    @NonNull
    Filter myFilter = new Filter() {

        @NonNull
        @Override
        protected FilterResults performFiltering(@Nullable CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<Countries> interestAndLanguageBeans = new ArrayList<Countries>();
            if (constraint != null && searchList != null) {
                int length = searchList.size();
                int i = 0;
                while (i < length) {
                    Countries countries = searchList.get(i);
                    if (countries.getCountryEnglishName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        interestAndLanguageBeans.add(countries);
                    }
                    i++;
                }
            }
            filterResults.values = interestAndLanguageBeans;
            filterResults.count = interestAndLanguageBeans.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, @NonNull FilterResults results) {
            countriesList = (List<Countries>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    };

    public CountryAdapter(Context activity, List<Countries> countriesList) {
        context = activity;
        this.countriesList = countriesList;
        searchList = countriesList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return countriesList.size();
    }

    @Override
    public Object getItem(int position) {
        return countriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, ViewGroup parent) {

        Handler handler = null;

        if (convertView == null) {
            handler = new Handler();
            convertView = layoutInflater.inflate(R.layout.activity_country_code_rowlayout, null);
            handler.tv_countryName = (TextView) convertView.findViewById(R.id.tv_countryName);
            handler.mRelativeLayoutRoot = (RelativeLayout) convertView.findViewById(R.id.rel_tv);
            convertView.setTag(handler);
        } else {
            handler = (Handler) convertView.getTag();
        }
        handler.tv_countryName.setTag(position);
        handler.tv_countryName.setText(countriesList.get(position).getCountryEnglishName());
        handler.mRelativeLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (int) v.getTag();
                String dialCode = countriesList.get(position).getCountryCode();
                String max_nsn = countriesList.get(position).getMax_NSN();
                ((ActivityCountryList) context).useDialCode(dialCode, max_nsn);
            }
        });


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    static class Handler {

        TextView tv_countryName;
        RelativeLayout mRelativeLayoutRoot;
    }
}



