package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.appstuds.heaven.Fragments.RandomPageFragment;
import com.gtomato.android.ui.widget.CarouselView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by osr on 7/3/18.
 */


public  class RandomPageAdapter extends CarouselView.Adapter<RandomPageAdapter.ViewHolder> {
    int size, width, height;

    public RandomPageAdapter(int size, int width, int height) {
        super();
        this.size = size;
        this.width = width;
        this.height = height;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = RandomPageFragment.createCircularImageView(context, width, height);
        ViewHolder holder = new ViewHolder(view);
        //View view = LayoutInflater.from(context).inflate(R.layout.carousel_container, parent);
        //return new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            /*TextView textView = (TextView) holder.itemView;
            Log.d(TAG, "onBindViewHolder: "+ holder.itemView.getId());*/
           /* if (position == 0){
                holder.circleImageView.setImageResource(drawable[position]);
                holder.circleImageView.setAlpha((float) 0.2);
                holder.textView.setText(size);
            }else holder.circleImageView.setImageResource(drawable[position]);*/


        RandomPageFragment.initializeImageView((CircleImageView) holder.itemView, (position ));


    }

    @Override
    public int getItemCount() {
        return size;
    }

    public  class ViewHolder extends CarouselView.ViewHolder {
        /*CircleImageView circleImageView;
        TextView textView;*/
        public ViewHolder(View itemView) {
            super(itemView);
            /*circleImageView = itemView.findViewById(R.id.circular_image_view);
            textView = itemView.findViewById(R.id.text_view);*/
        }
    }
}
