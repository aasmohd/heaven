package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.appstuds.heaven.Activities.HomeActivity;
import com.appstuds.heaven.Fragments.ProfileFragment;
import com.appstuds.heaven.R;

/**
 * Created by osr on 28/2/18.
 */


public class RecyclerAdapter_home extends RecyclerView.Adapter<RecyclerAdapter_home.RecyclerViewHolder> {

    Context context;
    View view;
    int [] img_res={R.drawable.man,R.drawable.man};




    public RecyclerAdapter_home(Context context) {

        this.context = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata_home, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.chatPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });


        holder.chatPersonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });










        holder.sideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showPopup(v);

            }
        });


        //for next card
//////////////////////////////////////////

        holder.second_chatPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });


        holder.second_chatPersonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });


        holder.second_sideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(context,People_business_Activity.class);
//                context.startActivity(intent);

                showPopup(v);

            }
        });


        /////////////////////

        holder.third_chatPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });


        holder.third_chatPersonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fm = ((HomeActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
            }
        });


        holder.third_sideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(context,People_business_Activity.class);
//                context.startActivity(intent);

                showPopup(v);

            }
        });





    }




    private void showPopup(View v) {



       // Context wrapper = new ContextThemeWrapper(context, R.style.popupMenuStyle);

        PopupMenu mypopupmenu = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            mypopupmenu = new PopupMenu(context,v);
        }
        MenuInflater inflater = mypopupmenu.getMenuInflater();
        inflater.inflate(R.menu.main, mypopupmenu.getMenu());

        mypopupmenu.show();
        mypopupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action1_1:

                        break;

                    case R.id.action1_2:


                        break;


                }

                return false;

            }
        });


    }



    @Override
    public int getItemCount() {

        return 4;

    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView chatPersonName,second_chatPersonName,third_chatPersonName;
        ImageView sideImage,second_sideImage,second_chatPersonImage,chatPersonImage,third_chatPersonImage,third_sideImage;
        CardView cardView;

        public RecyclerViewHolder(View View) {
            super(View);
            chatPersonName = (TextView) View.findViewById(R.id.chatpersonname);
            chatPersonImage = (ImageView) View.findViewById(R.id.chatpersonimage);

            second_chatPersonName = (TextView) View.findViewById(R.id.second_chatpersonname);
            second_chatPersonImage = (ImageView) View.findViewById(R.id.second_chatpersonimage);

            third_chatPersonName = (TextView) View.findViewById(R.id.third_chatpersonname);
            third_chatPersonImage = (ImageView) View.findViewById(R.id.third_chatpersonimage);

            cardView=(CardView)View.findViewById(R.id.cardview_home_item);
            sideImage=(ImageView)View.findViewById(R.id.sidelist);
            second_sideImage=(ImageView)View.findViewById(R.id.second_sidelist);
            third_sideImage=(ImageView)View.findViewById(R.id.third_sidelist);

        }
    }



}
