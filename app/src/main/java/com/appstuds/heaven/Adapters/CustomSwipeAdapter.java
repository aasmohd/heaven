package com.appstuds.heaven.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.appstuds.heaven.R;
import com.viewpagerindicator.CirclePageIndicator;


public class CustomSwipeAdapter extends PagerAdapter {


    private Context ctx;
    private LayoutInflater layoutInflater;
    private CirclePageIndicator indicator;

    int [] image_resources={R.drawable.tutorial_1,R.drawable.tutorial_2
            ,R.drawable.tutorial_3,R.drawable.tutorial_3};



    public CustomSwipeAdapter(Context ctx,CirclePageIndicator indicator) {
        this.ctx = ctx;
        this.indicator=indicator;


    }


    @Override
    public int getCount() {
       // return sliderlist.size();
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return (view == (LinearLayout) o);

    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(ctx);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
        ImageView imageview = (ImageView) item_view.findViewById(R.id.image_view);

        container.addView(item_view);
        imageview.setImageResource(image_resources[position]);

        if (position==3)
        {
            imageview.setVisibility(View.GONE);

        }




//        if (position==2)
//        {
//
//            Thread thread = new Thread() {
//
//
//                public void run() {
//
//                    try {
//                        sleep(1000);
//                        Intent intent=new Intent(ctx,LoginActivity.class);
//                        ctx.startActivity(intent);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//            };
//            thread.start();
//
//
//        }



//        Picasso.with(ctx).load(sliderlist.get(position).getSlide())
//
//                .into(imageview);
//
//        pass = (ItemClick) ctx;
//        pass.passShare(share);
//        pass.passDisc(disclaimer);
//
//        item_view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                pass = (ItemClick) ctx;
//                pass.clickAction(sliderlist.get(position).getUrl());
//
//
//            }
//        });


        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }



}
