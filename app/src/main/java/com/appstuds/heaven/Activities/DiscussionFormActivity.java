package com.appstuds.heaven.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appstuds.heaven.R;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Discussion;

public class DiscussionFormActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_discussion_form);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_discussion);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Discussion(this);
        recyclerView.setAdapter(adapter);
    }
}
