package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.Models.signupModel.MainFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;
import com.google.gson.JsonArray;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup_company_description_activity extends AppCompatActivity {
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_company_description_activity);
        type= Getter.getInstance().getBusiness_or_self();
    }

    public void selectpref(View view) {


        switch (type)
        {

            case 2:

              //  getResults();
                Intent intent=new Intent(Signup_company_description_activity.this,HomeActivity.class);
                startActivity(intent);
                finish();
                break;

            case 1:

                Intent intentt=new Intent(Signup_company_description_activity.this,SelectPreferenceActivity.class);
                startActivity(intentt);
                finish();
                break;


        }



    }


    private void getResults() {


        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        File file = new File("/storage/emulated/0/DCIM/man.png");

        String prefix ="";

        String firstname ="";
        String lastname = "";
        String prefname = "", email = Getter.getInstance().getEmail(), pass = Getter.getInstance().getPassword();
        String socialid = "asdfghjkl", logintype = Getter.getInstance().getLogintype(), bio ="", businessname = "b", mobile = "7351400862";

        JsonArray interest = new JsonArray();
        interest.add("");

        String gender = "male", accounttype = "1", devicetoken = "asdfghjkl";
        String address = "asdfghjkl", latitude = "28.5355", longitude = "77.3910";

        RequestBody requestprefix =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), prefix);
        RequestBody requestfirstname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), firstname);
        RequestBody requestlastname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), lastname);
        RequestBody requestprefname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), prefname);
        RequestBody requestemail =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), email);
        RequestBody requestpass =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), pass);
        RequestBody requestsocialid =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), socialid);
        RequestBody requestlogintype =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), logintype);
        RequestBody requestbio =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), bio);
        RequestBody requestbusinessname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), businessname);
        RequestBody requestmobile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mobile);

        RequestBody requestinterest =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), interest.toString());

        RequestBody requestgender =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), gender);

        RequestBody requestaccounttype =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), accounttype);

        RequestBody requestdevicetoken =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), devicetoken);

        RequestBody requestaddress =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), address);

        RequestBody requestlatitude =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), latitude);

        RequestBody requestlongitude =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), longitude);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));


        Call<MainFeed> call = apiServices.getResults(requestprefix, requestfirstname, requestlastname, requestprefname, requestemail, requestpass
                , requestsocialid, requestlogintype, requestbio, requestbusinessname, requestmobile, requestinterest, requestgender, requestaccounttype, requestdevicetoken, requestaddress, requestlatitude, requestlongitude, filePart);


        call.enqueue(new Callback<MainFeed>() {
            @Override
            public void onResponse(Call<MainFeed> call, Response<MainFeed> response) {

                MainFeed mainFeed = response.body();
                Log.d("apicheck", mainFeed.status + "");
                Intent intent=new Intent(Signup_company_description_activity.this,HomeActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void onFailure(Call<MainFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }
}
