package com.appstuds.heaven.Activities;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.appstuds.heaven.R;
//import com.appstuds.heaven.Fragments.ResourceFragment;

public class ResourcesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources);
        Launch();
    }

    private void Launch() {

        ResourceFragment fragment = new ResourceFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_res, fragment);
        ft.commit();
    }
}
