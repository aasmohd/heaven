package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.appstuds.heaven.Adapters.CustomSwipeAdapter;
import com.appstuds.heaven.R;
import com.viewpagerindicator.CirclePageIndicator;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    CustomSwipeAdapter customSwipeAdapter;
    CirclePageIndicator mIndicator;
    TextView title,desc;
    boolean count=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        customSwipeAdapter = new CustomSwipeAdapter(this,mIndicator);
        viewPager.setAdapter(customSwipeAdapter);
        mIndicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {




                                if (position == 2 && positionOffset>0.00  && count) {
                                    mIndicator.setVisibility(View.INVISIBLE);
                                    count=false;
                                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();

                                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int arg0) {
//
//                Log.d("check", arg0 + "");
//                if (arg0 == 3) {
//                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
//
//            }
//
//            @Override
//            public void onPageScrolled(int arg0, float arg1, int arg2) {
//
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int num) {
//
//
//            }
//        });


    }
}
