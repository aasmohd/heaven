package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstuds.heaven.Models.forgotpassmodel.ResetPassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ValidatePassFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassActivity extends AppCompatActivity {

    EditText reset_mailid;
    EditText resetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);

        reset_mailid =(EditText)findViewById(R.id.reset_mailid);
        resetPassword=(EditText)findViewById(R.id.reset_password);
        ImageView reset_ok_btn=(ImageView) findViewById(R.id.resetpass_ok_btn);
        reset_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });


    }

    private void resetPassword() {

        final String emailAddress=reset_mailid.getText().toString();
        final String otpString=resetPassword.getText().toString();





        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        Call<ResetPassFeed> call = apiServices.resetPassword(emailAddress,otpString);


        call.enqueue(new Callback<ResetPassFeed>() {
            @Override
            public void onResponse(Call<ResetPassFeed> call, Response<ResetPassFeed> response) {


                //  Log.d("loginapicheck", response.body().response.getUserprofile().getEmail());
                if (response.body().status==1)
                {
                    Toast.makeText(getApplicationContext(),"password change successfully",Toast.LENGTH_SHORT).show();



                }
                else if (response.body().status==0)
                {
                    Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResetPassFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }

}
