package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.appstuds.heaven.R;

public class Signup_compnay_name_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_compnay_name_activity);
    }

    public void opensignupphone(View view) {

        Intent intent=new Intent(Signup_compnay_name_activity.this,Signup_phone.class);
//        String url=getIntent().getExtras().getString("uri_image");
//        Uri imageUri = getIntent().getData();
//        intent.setData(imageUri);
//        intent.putExtra("uri_image",url);

        startActivity(intent);
        finish();
    }
}
