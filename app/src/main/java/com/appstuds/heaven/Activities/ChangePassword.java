package com.appstuds.heaven.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstuds.heaven.Models.changepassmodel.ChangePassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ResetPassFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    ImageView okbtn;
    String oldPassword,newpassword,confirmNewpass;
    EditText oldPass;
    EditText  newPass;
    EditText confirmNewPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        oldPass = (EditText) findViewById(R.id.old_password);
        newPass = (EditText) findViewById(R.id.new_password);
        confirmNewPass = (EditText) findViewById(R.id.confirm_new_password);

        okbtn = (ImageView) findViewById(R.id.changepass_ok_btn);


        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                oldPassword = oldPass.getText().toString();
                newpassword = newPass.getText().toString();
                confirmNewpass=confirmNewPass.getText().toString();

                if (newpassword.equals(confirmNewpass))
                {
                    changePass();
                }
                else Toast.makeText(getApplicationContext(),"Pass not Matching",Toast.LENGTH_SHORT).show();


            }
        });


    }

    private void changePass() {



        String access_token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VUb2tlbiI6ImFzZGZnaGprbCIsInVzZXJJZCI6IjVhYjhjODI0MjNmZWEwODUyOTkwMTM3YSIsInNvY2lhbElkIjoiYXNkZmdoamtsIiwiaWF0IjoxNTIyMDY2MDQ3fQ.QHAA_kUdWX6vRUFJHHrfoEhCuzJdr5znu9h5m7cTY-g";


        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        Call<ChangePassFeed> call = apiServices.changePass(oldPassword,newpassword,access_token);


        call.enqueue(new Callback<ChangePassFeed>() {
            @Override
            public void onResponse(Call<ChangePassFeed> call, Response<ChangePassFeed> response) {


                //  Log.d("loginapicheck", response.body().response.getUserprofile().getEmail());
                if (response.body().status == 1) {
                    Toast.makeText(getApplicationContext(), "password change successfully", Toast.LENGTH_SHORT).show();


                } else if (response.body().status == 0) {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ChangePassFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }


}

