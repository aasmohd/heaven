package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.Models.sharedprefmodel.SharedPrefFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup_description_activity extends AppCompatActivity {

    List<SharedPrefFeed.Response> interest=new ArrayList<>();
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_description_activity);
        type= Getter.getInstance().getBusiness_or_self();
//        getInterestList();


    }


    private void getInterestList() {

        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();


        Call<SharedPrefFeed> call = apiServices.getInterestList();


        call.enqueue(new Callback<SharedPrefFeed>() {
            @Override
            public void onResponse(Call<SharedPrefFeed> call, Response<SharedPrefFeed> response) {

                SharedPrefFeed Feed = response.body();
                //  Log.d("Sharedapicheck", Feed.response.get(0).getTitle() + "");

                for (int i=0;i<Feed.response.size();i++)
                {
                    interest.add(i,response.body().response.get(i));

                }


                Getter.getInstance().setInterest(interest);

                Log.d("Sharedapicheck", interest.get(0).getTitle() + "");

            }

            @Override
            public void onFailure(Call<SharedPrefFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });



    }

    public void opencompanydesc (View view) {

        switch (type)
        {

            case 2:

                //not usable
                Intent intent=new Intent(Signup_description_activity.this,Signup_compnay_name_activity.class);
                startActivity(intent);
                finish();
                break;

            case 1:

                Intent intentt=new Intent(Signup_description_activity.this,SelectPreferenceActivity.class);
                startActivity(intentt);
                finish();
                break;


        }


//        Intent intent=new Intent(Signup_description_activity.this,Signup_company_description_activity.class);
//
//        startActivity(intent);

    }
}
