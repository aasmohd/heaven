package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstuds.heaven.Models.forgotpassmodel.ForgotPassFeed;
import com.appstuds.heaven.Models.signinModel.SigninFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
    EditText mailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ImageView forgotpassok_btn=(ImageView)findViewById(R.id.forgotpass_ok_btn);
      mailId =(EditText)findViewById(R.id.forgotpass_mailid);
        forgotpassok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getForgotPassword();

            }
        });

    }


    private void getForgotPassword() {

            final String emailAddress=mailId.getText().toString();





            ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

            Call<ForgotPassFeed> call = apiServices.getForgotPassword(emailAddress);


            call.enqueue(new Callback<ForgotPassFeed>() {
                @Override
                public void onResponse(Call<ForgotPassFeed> call, Response<ForgotPassFeed> response) {


                    //  Log.d("loginapicheck", response.body().response.getUserprofile().getEmail());
                    if (response.body().status==1)
                    {
                        Toast.makeText(getApplicationContext(),"correct",Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(ForgotPasswordActivity.this,ValidateOTPActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    else if (response.body().status==0)
                    {
                        Toast.makeText(getApplicationContext(),"Email or Pass is incorrect",Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ForgotPassFeed> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                    Log.d("not", t.toString());

                }
            });

        }

}
