package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.appstuds.heaven.Fragments.MyProfileFragment;
import com.appstuds.heaven.R;

public class HomeFirstpostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_firstpost);

        ImageView imageView=(ImageView)findViewById(R.id.home_first_post_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                finish();
            }
        });
    }

    public void openChat(View view) {

//        Intent intent=new Intent(HomeFirstpostActivity.this,ChatActivity.class);
//        startActivity(intent);
    }
}
