package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.appstuds.heaven.Fragments.*;
import com.appstuds.heaven.R;
import com.appstuds.heaven.Adapters.RecyclerAdapter_business_profile;

public class BusinessProfileActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);
        //removed by testers
//        ImageView imageView=(ImageView)findViewById(R.id.business_prof_back);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                finish();
//            }
//        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_business_profile);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_business_profile(this);
        recyclerView.setAdapter(adapter);

    }
}
