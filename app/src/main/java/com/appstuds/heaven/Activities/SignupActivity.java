package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.Models.sharedprefmodel.SharedPrefFeed;
import com.appstuds.heaven.Models.validateemailfeed.ValidateEmailFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    EditText emailAdd, pass;
    ValidateEmailFeed Feed=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        TextView tv=(TextView)findViewById(R.id.spannable_signup);
      emailAdd =(EditText)findViewById(R.id.signup_emailAdd);
       pass=(EditText)findViewById(R.id.signup_pass);
        Button signup_btn=(Button) findViewById(R.id.logintype_email);



        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        final SpannableString text = new SpannableString("Already have an account ? Sign In");
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 26, 33, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(boldSpan, 26, 33, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(text);
    }


    private void validateEmail() {

        String email_Add=emailAdd.getText().toString();

        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();


        Call<ValidateEmailFeed> call = apiServices.validateEmail(email_Add);


        call.enqueue(new Callback<ValidateEmailFeed>() {
            @Override
            public void onResponse(Call<ValidateEmailFeed> call, Response<ValidateEmailFeed> response) {

                 Feed = response.body();

                if (Feed.status==1)
                {
                    Getter.getInstance().setEmail(emailAdd.getText().toString());
                    Getter.getInstance().setPassword(pass.getText().toString());
                    Getter.getInstance().setLogintype("1");
//                    Intent intent = new Intent(SignupActivity.this,Identifyyourself_Activity.class);
//                    startActivity(intent);
//                    finish();

                }
                else if (Feed.error.message.equals("Email already exist"))
                {
                    Toast.makeText(getApplicationContext(),"Email already exist",Toast.LENGTH_SHORT).show();
                }
                else if (Feed.error.message.equals("Validation errors"))
                {
                    Toast.makeText(getApplicationContext(),"Validation errors",Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ValidateEmailFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });



    }


    public void dosignup(View view) {

      //  validateEmail();
        Getter.getInstance().setEmail(emailAdd.getText().toString());
        Getter.getInstance().setPassword(pass.getText().toString());
        Getter.getInstance().setLogintype("1");


        Intent intent = new Intent(SignupActivity.this, Identifyyourself_Activity.class);
        startActivity(intent);
        finish();



    }

    public void openSigninpage(View view) {

        Intent intent=new Intent(SignupActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
