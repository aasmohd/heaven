package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.appstuds.heaven.Activities.MainActivity;
import com.appstuds.heaven.R;

public class Splash_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);



        Thread thread = new Thread() {


            public void run() {

                try {
                    sleep(1500);


                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        };
        thread.start();

    }
}
