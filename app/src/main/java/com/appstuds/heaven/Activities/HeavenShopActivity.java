package com.appstuds.heaven.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appstuds.heaven.R;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Shop;

public class HeavenShopActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heaven_shop);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_shop);
        layoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Shop(this);
        recyclerView.setAdapter(adapter);
    }
}
