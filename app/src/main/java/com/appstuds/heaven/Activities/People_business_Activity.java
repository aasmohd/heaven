package com.appstuds.heaven.Activities;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.appstuds.heaven.Fragments.PeopleBusinessHomeFragment;
import com.appstuds.heaven.R;

public class People_business_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_business_);

        HomeFragmentLaunch();


    }



    private void HomeFragmentLaunch() {

        PeopleBusinessHomeFragment fragment = new PeopleBusinessHomeFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_res, fragment);
        ft.commit();
    }


}
