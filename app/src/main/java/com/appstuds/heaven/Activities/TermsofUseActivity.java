package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.appstuds.heaven.R;

public class TermsofUseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsof_use);

//        ImageView imageView=(ImageView)findViewById(R.id.terms_of_use_back);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    public void openprivacypolicy(View view) {

        Intent intent=new Intent(TermsofUseActivity.this,PrivacyPolicyActivity.class);
        startActivity(intent);
    }
}
