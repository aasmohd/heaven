package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appstuds.heaven.Models.signinModel.SigninFeed;
import com.appstuds.heaven.Models.signupModel.MainFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    LoginButton fbloginButton;
    TextView textView,forgotpassword;
    CallbackManager callbackManager;
    EditText address,password;
    Button signinbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        forgotpassword=(TextView)findViewById(R.id.forgotpassword);
        address=(EditText)findViewById(R.id.signin_emailaddress);
        password=(EditText)findViewById(R.id.signin_password);
        fbloginButton=(LoginButton)findViewById(R.id.fb_login_bn);
        textView=(TextView)findViewById(R.id.check_status);
        signinbutton=(Button)findViewById(R.id.signin_loginbutton);
        TextView tv=(TextView)findViewById(R.id.spannable_login);

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(intent);
                finish();

            }
        });

        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        final SpannableString text = new SpannableString("Don't have an account ? Sign Up");
        text.setSpan(new RelativeSizeSpan(1.0f), text.length() - "Sign Up".length(), text.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        text.setSpan(boldSpan, 24, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv.setText(text);

//        signinbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getLoginResults();
//            }
//        });


    callbackManager=CallbackManager.Factory.create();
        fbloginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

//                textView.setText("Login success \n"+
//                        loginResult.getAccessToken().getUserId()+"\n"+loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                textView.setText("Login cancelled");

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }


    private void getLoginResults() {

        final String emailAddress=address.getText().toString();
        String emailpassword=password.getText().toString();

//        String email = "chaudhr1433569@gmail.com";
//        String password="asdfghjkl";
        String socialId="asdfghjkl";
        String loginType="1";
        String deviceToken="asdfghjkl";



        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        Call<SigninFeed> call = apiServices.getResultsSignin(emailAddress,emailpassword,socialId,loginType,deviceToken);


        call.enqueue(new Callback<SigninFeed>() {
            @Override
            public void onResponse(Call<SigninFeed> call, Response<SigninFeed> response) {


              //  Log.d("loginapicheck", response.body().response.getUserprofile().getEmail());
                if (response.body().status==1)
                {

                    Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();

                }
                else if (response.body().status==0)
                {
                    Toast.makeText(getApplicationContext(),"Email or Pass is in correct",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SigninFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);

    }

    public void opensignup(View view) {

        Intent intent=new Intent(LoginActivity.this,SignupActivity.class);
        startActivity(intent);
        finish();

    }

    public void openhomepage(View view) {

        Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();

    }

    public void forFb(View view) {
        fbloginButton.performClick();
    }
}
