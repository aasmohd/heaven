package com.appstuds.heaven.Activities;

import android.app.*;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appstuds.heaven.Adapters.CustomAdapterSpinner;
import com.appstuds.heaven.Models.CustomOnItemSelectedListener;
import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.R;

import java.util.ArrayList;
import java.util.List;

public class Signup_Name_Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    EditText prefixeditText;
    ArrayList<String> categories;
    EditText firstname,lastname,preffname,other_pleasefillin;
    Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup__name_);
        firstname=(EditText)findViewById(R.id.firstname);
       lastname=(EditText)findViewById(R.id.lastname);
        preffname =(EditText)findViewById(R.id.prefferedname);
        other_pleasefillin=(EditText)findViewById(R.id.other_fillin_editttext);




        // Spinner element

        spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(Signup_Name_Activity.this);

        // Spinner Drop down elements
       categories = new ArrayList<String>();
        categories.add("He");
        categories.add("She");
        categories.add("They");
        categories.add("Other (please fill in)");



        // Creating adapter for spinner
        CustomAdapterSpinner customAdapterSpinner = new CustomAdapterSpinner(Signup_Name_Activity.this, categories);

        // Drop down layout style - list view with radio button


        // attaching data adapter to spinner
        spinner.setAdapter(customAdapterSpinner);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String prefix=categories.get(position);

        if (position==3)
        {
            spinner.setVisibility(View.GONE);
            other_pleasefillin.setVisibility(View.VISIBLE);

        }

        Getter.getInstance().setPrefix(prefix);

    }
    public void onNothingSelected(AdapterView<?> arg0) {

    }





    public void opengender(View view) {

        Getter.getInstance().setFirstname(firstname.getText().toString());
        Getter.getInstance().setLastname(lastname.getText().toString());
        Getter.getInstance().setPrefferedname(preffname.getText().toString());

        Intent intent=new Intent(Signup_Name_Activity.this,Signup_upload_profile_activity.class);
        startActivity(intent);
        finish();
    }
}