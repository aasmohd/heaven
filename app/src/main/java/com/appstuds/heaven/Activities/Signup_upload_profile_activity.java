package com.appstuds.heaven.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.R;

public class Signup_upload_profile_activity extends AppCompatActivity implements View.OnClickListener {

    Button bt_upload;
    ImageView ivImage,defaultaddimage;
    Integer REQUEST_CAMERA=1,SELECT_FILE=0;
    int btncolor=Color.parseColor("#f79aac");
    Uri selectedImageUri;
    int type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_upload_profile_activity);
        type= Getter.getInstance().getBusiness_or_self();
        ivImage=(ImageView)findViewById(R.id.ivImage);
        defaultaddimage=(ImageView)findViewById(R.id.add_image);
        bt_upload=findViewById(R.id.bt_upload);
        bt_upload.setOnClickListener(this);

    }

    public void opencompanyname(View view) {

        switch (type)
        {

            case 2:

                Intent intent=new Intent(Signup_upload_profile_activity.this,Signup_compnay_name_activity.class);
                startActivity(intent);
                finish();
                break;

            case 1:

                Intent intentt=new Intent(Signup_upload_profile_activity.this,Signup_description_activity.class);
                startActivity(intentt);
                finish();
                break;


        }

//        Intent intent=new Intent(Signup_upload_profile_activity.this,Signup_compnay_name_activity.class);
//
//        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        Button button = (Button) v;


        // clear state
        bt_upload.setSelected(false);
        bt_upload.setPressed(false);

        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(btncolor);

        switch (v.getId())
        {
            case R.id.bt_upload:

                SelectImage();
                break;

        }
    }

    private void SelectImage()
    {
        final CharSequence[] items={"Camera","Gallery","Cancel"};

        AlertDialog.Builder builder=new AlertDialog.Builder(Signup_upload_profile_activity.this);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (items[which].equals("Camera"))
                {

                    Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,REQUEST_CAMERA);
                }
                else if (items[which].equals("Gallery"))
                {

                    Intent intent=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent,"Select File"),SELECT_FILE);

                }
                else if (items[which].equals("cancel"))
                {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data)
    {

        if (resultCode== Activity.RESULT_OK)
        {
            if (requestCode==REQUEST_CAMERA)
            {
                Bundle bundle=data.getExtras();
                final Bitmap bmp=(Bitmap) bundle.get("data");
                ivImage.setImageBitmap(bmp);
                defaultaddimage.setVisibility(View.GONE);
                ivImage.setVisibility(View.VISIBLE);
            }
            else if (requestCode==SELECT_FILE)
            {
                selectedImageUri =data.getData();
                ivImage.setImageURI(selectedImageUri);
                defaultaddimage.setVisibility(View.GONE);
                ivImage.setVisibility(View.VISIBLE);
            }
        }
    }

}
