package com.appstuds.heaven.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.appstuds.heaven.Models.Model;
import com.appstuds.heaven.Adapters.MultiViewTypeAdapter;
import com.appstuds.heaven.R;

import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity {
    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ImageView imageView=(ImageView)findViewById(R.id.chat_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       final ArrayList<Model> list= new ArrayList();
        list.add(new Model(Model.MSG_TYPE_RECEIVED,"Hi. I display a cool image too besides the omnipresent TextView.",1));
        final MultiViewTypeAdapter adapter = new MultiViewTypeAdapter(list,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

       final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);

        final EditText msgInputText = (EditText)findViewById(R.id.chat_input_msg);
        ImageView msgSendButton = (ImageView) findViewById(R.id.send_image);
        msgSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msgContent = msgInputText.getText().toString();
                if(!TextUtils.isEmpty(msgContent))
                {

                    int newMsgPosition = list.size()-1;
                    list.add(new Model(Model.MSG_TYPE_SENT,msgContent,newMsgPosition));
                    Log.d("check",newMsgPosition+"");
                    adapter.notifyDataSetChanged();
                    mRecyclerView.scrollToPosition(newMsgPosition);
                    msgInputText.setText("");
                }
            }
        });
    }

    public void openPost(View view) {

//        Intent intent=new Intent(ChatActivity.this,PostActivity.class);
//        startActivity(intent);
    }
}
