package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.R;

public class Identifyyourself_Activity extends AppCompatActivity implements View.OnClickListener {


     EditText otherspecify;
     Button otherButton,gaybtn,lesbianbtn,transbtn,queerbtn;
    int btncolor=Color.parseColor("#f79aac");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identifyyourself);


       otherspecify =(EditText)findViewById(R.id.other_specify_edittext);
        otherButton=findViewById(R.id.other_button);
        otherButton.setOnClickListener(this);


        gaybtn= findViewById(R.id.gaybtn);
        gaybtn.setOnClickListener(this);

        lesbianbtn=findViewById(R.id.lesbianbtn);
        lesbianbtn.setOnClickListener(this);

        transbtn=findViewById(R.id.transgenderbtn);
        transbtn.setOnClickListener(this);

        queerbtn=findViewById(R.id.queerbtn);
        queerbtn.setOnClickListener(this);




    }


    public void signupuploadpicture(View view) {

//        Intent intent=new Intent(Identifyyourself_Activity.this,Signup_upload_profile_activity.class);
//        startActivity(intent);
//        finish();
        Intent intent = new Intent(Identifyyourself_Activity.this, Signup_CreateAccount.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onClick(View v) {

        Button button = (Button) v;

        otherButton.setSelected(false);
        otherButton.setPressed(false);
        gaybtn.setSelected(false);
        gaybtn.setPressed(false);
        lesbianbtn.setSelected(false);
        lesbianbtn.setPressed(false);
        transbtn.setSelected(false);
        transbtn.setPressed(false);
        queerbtn.setSelected(false);
        queerbtn.setPressed(false);

        otherButton.setTextColor(Color.WHITE);
        gaybtn.setTextColor(Color.WHITE);
        lesbianbtn.setTextColor(Color.WHITE);
        transbtn.setTextColor(Color.WHITE);
        queerbtn.setTextColor(Color.WHITE);

        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(btncolor);

        switch (v.getId())
        {
            case R.id.other_button:
                otherspecify.setVisibility(View.VISIBLE);
                sendBio("Other");
                break;
            case R.id.gaybtn:
                otherspecify.setVisibility(View.INVISIBLE);
                sendBio("Gay");
                break;
            case R.id.lesbianbtn:
                otherspecify.setVisibility(View.INVISIBLE);
                sendBio("Lesbian");
                break;
            case R.id.transgenderbtn:
                otherspecify.setVisibility(View.INVISIBLE);
                sendBio("Transgender");
                break;
            case R.id.queerbtn:
                otherspecify.setVisibility(View.INVISIBLE);
                sendBio("Queer");
                break;

        }

    }

    private void sendBio(String bio) {

        Getter.getInstance().setBio(bio);

    }
}
