package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.Models.sharedprefmodel.SharedPrefFeed;
import com.appstuds.heaven.Models.signupModel.MainFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;
import com.google.gson.JsonArray;
import com.igalata.bubblepicker.BubblePickerListener;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectPreferenceActivity extends AppCompatActivity {
    BubblePicker bubblePicker;
    int count = 0;
    ApiServices apiServices;
    Uri myUri;
    List<SharedPrefFeed.Response> interest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_preference);
        bubblePicker = findViewById(R.id.picker);

      //  interest=Getter.getInstance().getInterest();


        initBubblePicker();



    }



    private void getResults() {


        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        File file = new File("/storage/emulated/0/DCIM/man.png");

        String prefix = Getter.getInstance().getPrefix();

        String firstname = Getter.getInstance().getFirstname();
        String lastname = Getter.getInstance().getLastname();
        String prefname =  Getter.getInstance().getPrefferedname(), email = Getter.getInstance().getEmail(), pass = Getter.getInstance().getPassword();
        String socialid = "asdfghjkl", logintype = Getter.getInstance().getLogintype(), bio = "male", businessname = "b", mobile = "7351400862";

        JsonArray interest = new JsonArray();
        interest.add("5aafb90f449fa4ac2871890a");

        String gender = "male", accounttype = "1", devicetoken = "asdfghjkl";
        String address = "asdfghjkl", latitude = "28.5355", longitude = "77.3910";

        RequestBody requestprefix =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), prefix);
        RequestBody requestfirstname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), firstname);
        RequestBody requestlastname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), lastname);
        RequestBody requestprefname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), prefname);
        RequestBody requestemail =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), email);
        RequestBody requestpass =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), pass);
        RequestBody requestsocialid =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), socialid);
        RequestBody requestlogintype =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), logintype);
        RequestBody requestbio =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), bio);
        RequestBody requestbusinessname =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), businessname);
        RequestBody requestmobile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mobile);

        RequestBody requestinterest =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), interest.toString());

        RequestBody requestgender =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), gender);

        RequestBody requestaccounttype =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), accounttype);

        RequestBody requestdevicetoken =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), devicetoken);





        RequestBody requestaddress =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), address);

        RequestBody requestlatitude =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), latitude);

        RequestBody requestlongitude =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), longitude);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));


        Call<MainFeed> call = apiServices.getResults(requestprefix, requestfirstname, requestlastname, requestprefname, requestemail, requestpass
                , requestsocialid, requestlogintype, requestbio, requestbusinessname, requestmobile, requestinterest, requestgender, requestaccounttype, requestdevicetoken, requestaddress, requestlatitude, requestlongitude, filePart);


        call.enqueue(new Callback<MainFeed>() {
            @Override
            public void onResponse(Call<MainFeed> call, Response<MainFeed> response) {

                MainFeed mainFeed = response.body();
                Log.d("apicheck", mainFeed.status + "");
                Intent intent=new Intent(SelectPreferenceActivity.this,HomeActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void onFailure(Call<MainFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }


    private void initBubblePicker() {


        final String[] titles = getResources().getStringArray(R.array.countries);
        final TypedArray colors = getResources().obtainTypedArray(R.array.colors);
        final TypedArray colorsSelected = getResources().obtainTypedArray(R.array.colors_selected);

        final TypedArray images = getResources().obtainTypedArray(R.array.images);

        bubblePicker.setAdapter(new BubblePickerAdapter() {
            @Override
            public int getTotalCount() {
                return titles.length;
            }

            @NotNull
            @Override
            public PickerItem getItem(int i) {
                PickerItem pickerItem = new PickerItem();
                pickerItem.setTitle(titles[i].toUpperCase());

                pickerItem.setGradient(new BubbleGradient(colors.getColor((i * 2) % 8, 0),
                        colors.getColor((i * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
                pickerItem.setTextColor(ContextCompat.getColor(SelectPreferenceActivity.this, android.R.color.white));
                pickerItem.setColor(Color.BLUE);
                pickerItem.setBackgroundImage(ContextCompat.getDrawable(SelectPreferenceActivity.this, images.getResourceId(i, 0)));
                return pickerItem;
            }
        });

        colors.recycle();
        images.recycle();


        bubblePicker.setBubbleSize(80);
        bubblePicker.setMaxSelectedCount(5);
        bubblePicker.getCenterImmediately();
        bubblePicker.setListener(new BubblePickerListener() {
            @Override
            public void onBubbleSelected(PickerItem pickerItem) {
                count++;


             //   Toast.makeText(SelectPreferenceActivity.this, pickerItem.getTitle(), Toast.LENGTH_SHORT).show();
               pickerItem.setTypeface(Typeface.create("abel_regular", Typeface.NORMAL));
                pickerItem.setGradient(new BubbleGradient(colorsSelected.getColor(0, 0),
                        colorsSelected.getColor(0, 0), BubbleGradient.VERTICAL));

                if (count == 5) {
                //    getResults();
                    Intent intent = new Intent(SelectPreferenceActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();


                }

            }

            @Override
            public void onBubbleDeselected(PickerItem pickerItem) {

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        bubblePicker.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bubblePicker.onPause();
    }

    public void openhome(View view) {

//        Intent intent = new Intent(SelectPreferenceActivity.this, HomeActivity.class);
//        startActivity(intent);
//        finish();
    }
}
