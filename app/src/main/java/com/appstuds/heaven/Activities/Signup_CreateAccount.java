package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.R;

public class Signup_CreateAccount extends AppCompatActivity implements View.OnClickListener {

    Button mButton1,mButton2;
    int btncolor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup__create_account);
         btncolor=Color.parseColor("#f79aac");
        mButton1 = findViewById(R.id.bt_myself);
        mButton2 = findViewById(R.id.bt_mybusiness);

        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Button button = (Button) v;



        // clear state
        mButton1.setSelected(false);
        mButton1.setPressed(false);
        mButton2.setSelected(false);
        mButton2.setPressed(false);
        mButton1.setTextColor(Color.WHITE);
        mButton2.setTextColor(Color.WHITE);


        // change state
        button.setSelected(true);
        button.setPressed(false);
        button.setTextColor(btncolor);


        switch (v.getId())
        {
            case R.id.bt_myself:
                Intent intent=new Intent(Signup_CreateAccount.this,Signup_Name_Activity.class);
                Getter.getInstance().setBusiness_or_self(1);
                startActivity(intent);
                finish();

                break;

            case R.id.bt_mybusiness:
                Intent intentt=new Intent(Signup_CreateAccount.this,Signup_upload_profile_activity.class);
                Getter.getInstance().setBusiness_or_self(2);
                startActivity(intentt);
                finish();

                break;
        }


    }




}
