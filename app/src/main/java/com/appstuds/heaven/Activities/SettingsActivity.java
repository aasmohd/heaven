package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.appstuds.heaven.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
    }

    public void opentermsofuse(View view) {

        Intent intent=new Intent(SettingsActivity.this,TermsofUseActivity.class);
        startActivity(intent);
    }
}
