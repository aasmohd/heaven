package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.appstuds.heaven.Models.Getter;
import com.appstuds.heaven.Utils.ActivityCountryList;
import com.appstuds.heaven.R;

public class Signup_phone extends AppCompatActivity {
    String code;
    EditText editText;
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_phone);
       editText=(EditText)findViewById(R.id.country_code);
       type= Getter.getInstance().getBusiness_or_self();


    }

    public void opensignupdescription(View view) {

        switch (type)
        {

            case 2:

                Intent intent = new Intent(Signup_phone.this, Signup_company_description_activity.class);
                startActivity(intent);
                finish();
                break;

            case 1:

                Intent intentt = new Intent(Signup_phone.this, Signup_description_activity.class);
                startActivity(intentt);
                finish();
                break;


        }






    }

    public void counrtyCode(View view) {

        Intent intent = new Intent(Signup_phone.this, ActivityCountryList.class);
        startActivityForResult(intent, 5525);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 5525) {
                code = data.getStringExtra("code");
                Log.d("cdoe", code);
                editText.setText("+"+code);
            }
        }
    }
}
