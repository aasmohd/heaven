package com.appstuds.heaven.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.appstuds.heaven.Adapters.RandomPageAdapter;
import com.appstuds.heaven.R;
import com.appstuds.heaven.Adapters.RecyclerAdapter_Discussion_open;
import com.gtomato.android.ui.transformer.TimeMachineViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;

import static android.content.ContentValues.TAG;

public class DiscussionForumOpen extends AppCompatActivity {
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;
    CarouselView carouselView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_forum_open);

        ImageView imageView=(ImageView)findViewById(R.id.discussion_open_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
     //   carouselView = (CarouselView)findViewById(R.id.carousel_view);
      //  setCarsoleView();


        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_discussion_open);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter_Discussion_open(this);
        recyclerView.setAdapter(adapter);
    }


    private void setCarsoleView() {


        carouselView.setAdapter(new RandomPageAdapter(4, 30,30));
        carouselView.setExtraVisibleChilds(0);
        carouselView.setGravity(Gravity.CENTER);
        carouselView.setOnItemSelectedListener(new CarouselView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {
                Log.d(TAG,"Selected Position " + position);
            }

            @Override
            public void onItemDeselected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {

            }
        });

        carouselView.setTransformer(new TimeMachineViewTransformer());
    }
}
