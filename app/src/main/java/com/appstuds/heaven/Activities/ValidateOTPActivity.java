package com.appstuds.heaven.Activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstuds.heaven.Models.forgotpassmodel.ForgotPassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ValidatePassFeed;
import com.appstuds.heaven.R;
import com.appstuds.heaven.network.ApiServices;
import com.appstuds.heaven.network.AppRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidateOTPActivity extends AppCompatActivity {
    EditText forgotpass_mailid;
    EditText otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_otp);

       forgotpass_mailid =(EditText)findViewById(R.id.forgotpass_mailid_validate);
        otp=(EditText)findViewById(R.id.otp);
        ImageView validate_ok_btn=(ImageView) findViewById(R.id.validatepass_ok_btn);
        validate_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateOTP();
            }
        });


    }


    private void validateOTP() {

        final String emailAddress=forgotpass_mailid.getText().toString();
        final String otpString=otp.getText().toString();





        ApiServices apiServices = AppRetrofit.getInstance().getApiServices();

        Call<ValidatePassFeed> call = apiServices.validateOTP(emailAddress,otpString);


        call.enqueue(new Callback<ValidatePassFeed>() {
            @Override
            public void onResponse(Call<ValidatePassFeed> call, Response<ValidatePassFeed> response) {


                //  Log.d("loginapicheck", response.body().response.getUserprofile().getEmail());
                if (response.body().status==1)
                {
                    Toast.makeText(getApplicationContext(),"correct",Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(ValidateOTPActivity.this,ResetPassActivity.class);
                    startActivity(intent);
                    finish();

                }
                else if (response.body().status==0)
                {
                    Toast.makeText(getApplicationContext(),"Email or Pass is incorrect",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ValidatePassFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.d("not", t.toString());

            }
        });

    }

}
