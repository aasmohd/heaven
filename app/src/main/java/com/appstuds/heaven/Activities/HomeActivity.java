package com.appstuds.heaven.Activities;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appstuds.heaven.Fragments.DiscussionForumFragment;
import com.appstuds.heaven.Fragments.DiscussionForumOpenFragment;
import com.appstuds.heaven.Fragments.HeavenShopFragment;
import com.appstuds.heaven.Fragments.HomeFirstPostFragment;
import com.appstuds.heaven.Fragments.HomeFragment;
import com.appstuds.heaven.Fragments.InboxFragment;
import com.appstuds.heaven.Fragments.MyProfileFragment;
import com.appstuds.heaven.Fragments.PostFragment;
import com.appstuds.heaven.Fragments.PrivacyPolicyFragment;
import com.appstuds.heaven.Fragments.SettingFragment;
import com.appstuds.heaven.Fragments.TermsOfUseFragment;
import com.appstuds.heaven.R;
import com.appstuds.heaven.Utils.AppUtils;
import com.appstuds.heaven.interfaces.PassSelector;
import com.appstuds.heaven.interfaces.ShowBottomBar;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, PassSelector, ShowBottomBar {
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;
    RelativeLayout relativeLayout;
    RelativeLayout relativeLayout2;
    boolean doubleBackToExitPressedOnce = false;
    FragmentTransaction ft;
    FragmentManager fm;
    ViewGroup root;
    int count = 0;
    //
    ImageView iv_home, iv_profile, imageaddpost, imageinbox, iv_more;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        root = (ViewGroup) getWindow().getDecorView().getRootView();

        //
        iv_home = findViewById(R.id.iv_home);
        relativeLayout = (RelativeLayout) findViewById(R.id.rel_bottom_bar);
        iv_profile = findViewById(R.id.iv_profile);
        imageaddpost = findViewById(R.id.iv_add);
        imageinbox = findViewById(R.id.iv_inbox);
        final LinearLayout linearLayout = findViewById(R.id.iv_more_container);
        linearLayout.setOnClickListener(this);
        iv_more = findViewById(R.id.iv_more);
        iv_home.setOnClickListener(this);
        iv_profile.setOnClickListener(this);
        imageaddpost.setOnClickListener(this);
        imageinbox.setOnClickListener(this);


        HomeFragmentLaunch();

    }


    public void themechange(boolean theme) {
        if (theme) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window w = getWindow(); // in Activity's onCreate() for instance
                w.clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                // w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                Fragment fragment = fm.findFragmentById(R.id.container_main);
                if (fragment instanceof HomeFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                } else if (fragment instanceof SettingFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof HeavenShopFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof ResourceFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof InboxFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof PostFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof DiscussionForumOpenFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof DiscussionForumFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof PrivacyPolicyFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof TermsOfUseFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
                else if (fragment instanceof HomeFirstPostFragment) {
                    window.setStatusBarColor(ContextCompat.getColor(this, R.color.lightblue));
                }
            }
        }
    }

    @Override
    public void onClick(View vv) {


        if (vv instanceof ImageView) {
            ImageView imageView = (ImageView) vv;


            iv_home.setSelected(false);
            iv_home.setPressed(false);
            iv_profile.setSelected(false);
            iv_profile.setPressed(false);
            imageaddpost.setSelected(false);
            imageaddpost.setPressed(false);
            imageinbox.setSelected(false);
            imageinbox.setPressed(false);
            iv_more.setPressed(false);
            iv_more.setSelected(false);

            imageView.setSelected(true);
            imageView.setPressed(false);

        }
        switch (vv.getId()) {

            case R.id.iv_home:

                HomeFragment fragment = new HomeFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
                break;

            case R.id.iv_profile:

                ResourceFragment fragmentt = new ResourceFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmentt);
                ft.commit();
                break;

            case R.id.iv_add:

                PostFragment fragmenttt = new PostFragment();
                 fm = getSupportFragmentManager();
                 ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmenttt);
                ft.commit();
                break;

            case R.id.iv_inbox:

                InboxFragment fragmentinbox = new InboxFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmentinbox);
                ft.commit();
                break;

            case R.id.iv_more_container:
                setColor_ivmore();
                // showPopup(vv);
                showpopUp(vv);

                break;

        }


    }

    private void setColor_ivmore() {


        iv_home.setSelected(false);
        iv_home.setPressed(false);
        iv_profile.setSelected(false);
        iv_profile.setPressed(false);
        imageaddpost.setSelected(false);
        imageaddpost.setPressed(false);
        imageinbox.setSelected(false);
        imageinbox.setPressed(false);

        iv_more.setSelected(true);
        iv_more.setPressed(false);


    }


    public void showpopUp(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom);
        TextView myprofile = (TextView) dialog.findViewById(R.id.idmyprofile);
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyProfileFragment fragment = new MyProfileFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragment);
                ft.commit();
                dialog.dismiss();
                setColor_myprofile();

            }
        });

        TextView shop = (TextView) dialog.findViewById(R.id.idshop);
        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HeavenShopFragment fragmentt = new HeavenShopFragment();
                fm = getSupportFragmentManager();
                 ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmentt);
                ft.commit();
                dialog.dismiss();
            }
        });

        TextView discussion = (TextView) dialog.findViewById(R.id.iddiscussion);
        discussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DiscussionForumFragment fragmenttt = new DiscussionForumFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmenttt);
                ft.commit();
                dialog.dismiss();

            }
        });

        TextView settings = (TextView) dialog.findViewById(R.id.idsettings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SettingFragment fragmentttt = new SettingFragment();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container_main, fragmentttt);
                ft.commit();
                dialog.dismiss();

            }
        });

        dialog.getWindow().setGravity(Gravity.END | Gravity.BOTTOM);
        dialog.getWindow().getAttributes().y = AppUtils.dpToPx(50);
        dialog.getWindow().getAttributes().x = AppUtils.dpToPx(0);
        dialog.show();


    }


    private void setColor_myprofile() {

        iv_home.setSelected(true);
        iv_home.setPressed(false);
        iv_profile.setSelected(false);
        iv_profile.setPressed(false);
        imageaddpost.setSelected(false);
        imageaddpost.setPressed(false);
        imageinbox.setSelected(false);
        imageinbox.setPressed(false);

        iv_more.setSelected(false);
        iv_more.setPressed(false);

    }

    private void HomeFragmentLaunch() {
        setDefaultHometabcolor();
        HomeFragment fragment = new HomeFragment();
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.replace(R.id.container_main, fragment);
        ft.commit();
    }

    private void setDefaultHometabcolor() {

        iv_more.setSelected(false);
        iv_more.setPressed(false);
        iv_profile.setSelected(false);
        iv_profile.setPressed(false);
        imageaddpost.setSelected(false);
        imageaddpost.setPressed(false);
        imageinbox.setSelected(false);
        imageinbox.setPressed(false);

        iv_home.setSelected(true);
        iv_home.setPressed(false);


    }

    @Override
    public void passData(int i) {

        if (i == 1) {

            HomeFragmentLaunch();

            iv_more.setSelected(false);
            iv_more.setPressed(false);
            iv_profile.setSelected(false);
            iv_profile.setPressed(false);
            imageaddpost.setSelected(false);
            imageaddpost.setPressed(false);
            imageinbox.setSelected(false);
            imageinbox.setPressed(false);

            iv_home.setSelected(true);
            iv_home.setPressed(false);
        }


    }

    @Override
    public void displayBottomBar() {
        //  relativeLayout.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {

        showVisibleBottomBar(false);
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void showVisibleBottomBar(Boolean aBoolean) {
        if (aBoolean) {
            relativeLayout.setVisibility(View.GONE);
        } else {
            relativeLayout.setVisibility(View.VISIBLE);
        }
    }


//    public void openprofile(View view) {
//
//        Intent intent=new Intent(HomeActivity.this,ProfileActivity.class);
//        startActivity(intent);
//    }
}
