package com.appstuds.heaven.Utils;

import android.support.annotation.NonNull;

import com.appstuds.heaven.Utils.Countries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Dragonfly on 6/4/2016.
 */
public class JsonParserHelper {


    @NonNull
    public static ArrayList<Countries> getCountry(String countryJson) throws JSONException {
        ArrayList<Countries> countries = new ArrayList<Countries>();
        JSONArray country_code = new JSONArray(countryJson);

        try {

            for (int i = 0; i < country_code.length(); i++) {

                Countries bean = new Countries();

                JSONObject object = country_code.getJSONObject(i);

                String name = object.getString("CountryEnglishName");
                String dail_code = object.getString("CountryCode");
                String code = object.getString("Max NSN");

                bean.setCountryEnglishName(name);
                bean.setCountryCode(dail_code);
                bean.setMax_NSN(code);

                countries.add(bean);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countries;
    }

}
