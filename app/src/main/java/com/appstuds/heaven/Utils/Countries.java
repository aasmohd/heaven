package com.appstuds.heaven.Utils;

/**
 * Created by Dragonfly on 6/4/2016.
 */
public class Countries {
    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getCountryEnglishName() {
        return CountryEnglishName;
    }

    public void setCountryEnglishName(String countryEnglishName) {
        CountryEnglishName = countryEnglishName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getMax_NSN() {
        return Max_NSN;
    }

    public void setMax_NSN(String max_NSN) {
        Max_NSN = max_NSN;
    }

    public String getAppleStore() {
        return AppleStore;
    }

    public void setAppleStore(String appleStore) {
        AppleStore = appleStore;
    }

    public String getCountryExitCode() {
        return CountryExitCode;
    }

    public void setCountryExitCode(String countryExitCode) {
        CountryExitCode = countryExitCode;
    }

    public String getCountryLocalName() {
        return CountryLocalName;
    }

    public void setCountryLocalName(String countryLocalName) {
        CountryLocalName = countryLocalName;
    }

    public String getGoogleStore() {
        return GoogleStore;
    }

    public void setGoogleStore(String googleStore) {
        GoogleStore = googleStore;
    }

    public String getmISOCode() {
        return mISOCode;
    }

    public void setmISOCode(String mISOCode) {
        this.mISOCode = mISOCode;
    }

    public String getMaxNSN() {
        return MaxNSN;
    }

    public void setMaxNSN(String maxNSN) {
        MaxNSN = maxNSN;
    }

    public String getMinNSN() {
        return MinNSN;
    }

    public void setMinNSN(String minNSN) {
        MinNSN = minNSN;
    }

    public String getMobileRank() {
        return MobileRank;
    }

    public void setMobileRank(String mobileRank) {
        MobileRank = mobileRank;
    }

    public String getSortIndex() {
        return SortIndex;
    }

    public void setSortIndex(String sortIndex) {
        SortIndex = sortIndex;
    }

    public String getTrunkCode() {
        return TrunkCode;
    }

    public void setTrunkCode(String trunkCode) {
        TrunkCode = trunkCode;
    }

    public String getUNRank() {
        return UNRank;
    }

    public void setUNRank(String UNRank) {
        this.UNRank = UNRank;
    }

    private String CountryID, CountryEnglishName, CountryCode, Max_NSN, AppleStore, CountryExitCode, CountryLocalName, GoogleStore, mISOCode, MaxNSN, MinNSN, MobileRank, SortIndex, TrunkCode, UNRank;

}
