package com.appstuds.heaven.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.appstuds.heaven.Adapters.CountryAdapter;
import com.appstuds.heaven.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sukhbeer on 6/4/2016.
 */
public class ActivityCountryList extends AppCompatActivity {

    @Nullable
    public String mStringcountryJSON;
    CountryAdapter mCountryAdapter;
    List<Countries> mCountriesList;
    private EditText mEditTextSelectCountry;
    private ListView mListViewCountry;
    private RelativeLayout mRelativeLayoutCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_layout);
        initUI();
        listener();
        mStringcountryJSON = AppUtils.jsonToStringFromAssetFolder("CountryCodes.json", getApplicationContext());
        new GetList(ActivityCountryList.this, mStringcountryJSON).execute();
        editTextWatcher();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void editTextWatcher() {
        mEditTextSelectCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(@NonNull CharSequence s, int start, int before, int count) {
                mCountryAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /*
    * Method to use the Dial code of all the countries.
    * */

    public void useDialCode(String dialCode, String max_nsn) {
        AppUtils.hideKeyBoard(ActivityCountryList.this);
        Intent intent = new Intent();
        intent.putExtra("code", dialCode);
        intent.putExtra("MAX_NSN", max_nsn);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void use_max_nsn(String max_nsn) {

    }

    /*
    * Method to initialize all the Views.
    * */


    public void initUI() {
        mEditTextSelectCountry = (EditText) findViewById(R.id.etv_search_countrry);
        mListViewCountry = (ListView) findViewById(R.id.lv_country);
        mRelativeLayoutCancel = (RelativeLayout) findViewById(R.id.rel_cancel);
    }

    /*
    * Method to apply listener on the views.
    * */
    public void listener() {
        mRelativeLayoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTextSelectCountry.setText("");
                AppUtils.hideKeyBoard(ActivityCountryList.this);
                finish();
            }
        });
    }


    /*
    * Method to load the Country list.
    * */
    public void looadList(List<Countries> countriesList) {
        mCountriesList = countriesList;
        mCountryAdapter = new CountryAdapter(ActivityCountryList.this, countriesList);
        mListViewCountry.setAdapter(mCountryAdapter);
    }

}


/*
*
* Class to get the list of the Countries which extends the Async Task.
* */
@SuppressWarnings("ALL")
class GetList extends AsyncTask<Void, Void, List<Countries>> {
    String countryJSON;
    Context context;
    List<Countries> countriesArrayList = new ArrayList<>();

    public GetList(Context context, String countryJSON) {
        this.countryJSON = countryJSON;
        this.context = context;
    }

    @NonNull
    @Override
    protected List<Countries> doInBackground(Void... params) {
        try {
            JsonParserHelper parserHelper = new JsonParserHelper();
            countriesArrayList = JsonParserHelper.getCountry(countryJSON);
        } catch (Exception ignored) {

        }
        return countriesArrayList;
    }

    @Override
    protected void onPostExecute(List<Countries> countriesList) {
        super.onPostExecute(countriesList);
        ((ActivityCountryList) context).looadList(countriesList);

    }
}
