package com.appstuds.heaven.network;


import android.text.TextUtils;

import com.facebook.internal.Utility;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class AppRetrofit {
    private static AppRetrofit mInstance;
    ApiServices apiServices;

    private AppRetrofit() {
        // For logging
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new ForbiddenInterceptor())
                .connectTimeout(120000, TimeUnit.SECONDS)
                .readTimeout(120000, TimeUnit.SECONDS)
                .writeTimeout(120000, TimeUnit.SECONDS)
                .build();

        // Rest adapter
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .create()))
                .build();

        apiServices = retrofit.create(ApiServices.class);
    }

    public static synchronized AppRetrofit getInstance() {
        if (mInstance == null)
            mInstance = new AppRetrofit();
        return mInstance;
    }

    private Interceptor addHeaders() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder request = chain.request().newBuilder();

                String accessToken = "sterytyeryery";
                if (!TextUtils.isEmpty(accessToken))
                    request.addHeader("accessToken", accessToken);
                return chain.proceed(request.build());
            }
        };
    }

    private class ForbiddenInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
            return response;
        }
    }

    public ApiServices getApiServices() {
        return apiServices;
    }

}


