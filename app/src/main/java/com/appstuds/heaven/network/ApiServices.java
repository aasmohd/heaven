package com.appstuds.heaven.network;

import com.appstuds.heaven.Models.changepassmodel.ChangePassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ForgotPassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ResetPassFeed;
import com.appstuds.heaven.Models.forgotpassmodel.ValidatePassFeed;
import com.appstuds.heaven.Models.sharedprefmodel.SharedPrefFeed;
import com.appstuds.heaven.Models.signinModel.SigninFeed;
import com.appstuds.heaven.Models.signupModel.MainFeed;
import com.appstuds.heaven.Models.validateemailfeed.ValidateEmailFeed;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by osr on 20/3/18.
 */

public interface ApiServices {

    @Multipart
    @POST("user/signup")
    public Call<MainFeed> getResults(@Part("prefix")RequestBody prefix,@Part("firstName")RequestBody firstname
    , @Part("lastName")RequestBody lastname, @Part("preferredName")RequestBody preferredname,@Part("email")RequestBody email
    , @Part("password")RequestBody password, @Part("socialId")RequestBody socialid, @Part("loginType")RequestBody logintype
    , @Part("bio")RequestBody bio, @Part("businessName")RequestBody businessname,@Part("mobile")RequestBody mobile
    , @Part("interests")RequestBody interests, @Part("gender")RequestBody gender,@Part("accountType")RequestBody accounttype
    ,@Part("deviceToken")RequestBody devicetoken,@Part("address")RequestBody address,@Part("latitude")RequestBody latitude
    ,@Part("longitude")RequestBody longitude,@Part MultipartBody.Part filePart);

    @FormUrlEncoded
    @POST("user/login")
    public Call<SigninFeed>getResultsSignin(@Field("email")String email,@Field("password")String password
    ,@Field("socialId")String socialid,@Field("loginType")String logintype,@Field("deviceToken")String devicetoken);

    @POST("user/getInterestList")
    public Call<SharedPrefFeed>getInterestList();


    @FormUrlEncoded
    @POST("user/forgetPassword")
    public Call<ForgotPassFeed>getForgotPassword(@Field("email")String email);

    @FormUrlEncoded
    @POST("user/validateOtp")
    public Call<ValidatePassFeed>validateOTP(@Field("email")String email,@Field("otp")String otp);


    @FormUrlEncoded
    @POST("user/resetPassword")
    public Call<ResetPassFeed>resetPassword(@Field("email")String email, @Field("password")String password);

    @FormUrlEncoded
    @POST("user/changePassword")
    public Call<ChangePassFeed>changePass(@Field("oldPassword")String oldpassword, @Field("newPassword")String newpassword, @Header("accessToken")String access_token);

    @FormUrlEncoded
    @POST("user/validateEmail")
    public Call<ValidateEmailFeed>validateEmail(@Field("email")String email);





}

